# File-ID
A reference for file identification

## Project origin
Multiple times I tried to work with files in the web or localy, whereas I needed to identify the type of which. Given these projects I figured using the extension was good enough. However how do I get the project to know which file is which?

So this is a machine readable reference which can be included in the project.

## Usage
Download the ***files.json*** file. Transform it to whatever format is needed and include it in your project.